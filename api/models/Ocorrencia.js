module.exports = {
    attributes: {
        createdAt: false,
        updatedAt: false,
        codigoOcorrencia: {
            type: 'number',
            columnName: 'Código da Ocorrência',
            required: true
        },
        classificacaoOcorrencia: {
            type: 'string',
            required: true
        },
        cidadeOcorrencia: {
            type: 'string',
            required: true
        },
        ufOcorrencia: {
            type: 'string',
            required: true
        },
        paisOcorrencia: {
            type: 'string',
            required: true
        },
        dataOcorrencia: {
            type: 'string',
            required: true
        },
        horaOcorrencia: {
            type: 'string',
            required: true
        },
        relatorioDivulgado: {
            type: 'string',
            required: true
        },
        recomendacoesRelatorio: {
            type: 'number',
            required: true
        },
        quantidadeAeronaves: {
            type: 'number',
            required: true
        },
        fatores: {
            collection: 'FatorOcorrencia',
            via: 'codigoOcorrencia'
        },
        aeronaves: {
            collection: 'AeronavesOcorrencia',
            via: 'codigoOcorrencia'
        },
        recomendacoes: {
            collection: 'RecomendacaoOcorrencia',
            via: 'codigoOcorrencia'
        },
        tipos: {
            collection: 'TipoOcorrencia',
            via: 'codigoOcorrencia'
        }
    }
}