module.exports = {
    attributes: {
        createdAt: false,
        updatedAt: false,
        codigoOcorrencia: {
            model: 'Ocorrencia',
            columnName: 'Código da Ocorrência',
            required: true
        },
        nomeFator: {
            type: 'string',
            required: true
        },
        aspectoFator: {
            type: 'string',
            required: true
        },
        fatorCondicionante: {
            type: 'string',
            required: true
        },
        fatorArea: {
            type: 'string',
            required: true
        }
    }
}