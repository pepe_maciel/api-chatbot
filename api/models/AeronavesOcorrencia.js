module.exports = {
    attributes: {
        createdAt: false,
        updatedAt: false,
        codigoOcorrencia: {
            model: 'Ocorrencia',
            columnName: 'Código da Ocorrência',
            required: true
        },
        aeronaveMatricula: {
            type: 'string',
            required: true
        },
        aeronaveTipo: {
            type: 'string',
            required: true
        },
        aeronaveFabricante: {
            type: 'string',
            required: true
        },
        aeronaveModelo: {
            type: 'string',
            required: true
        },
        aeronaveAno: {
            type: 'number',
            required: true
        },
        aeronavePaisFabricante: {
            type: 'string',
            required: true
        },
        aeronaveSegmento: {
            type: 'string',
            required: true
        },
        aeronaveFaseOperacao: {
            type: 'string',
            required: true
        }
    }
}