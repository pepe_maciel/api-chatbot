module.exports = {
    attributes: {
        createdAt: false,
        updatedAt: false,
        codigoOcorrencia: {
            model: 'Ocorrencia',
            columnName: 'Código da Ocorrência',
            required: true
        },
        recomendacaoNumero: {
            type: 'string',
            required: true
        },
        recomendacaoConteudo: {
            type: 'string',
            required: true
        },
        recomendacaoDestinatario: {
            type: 'string',
            required: true
        }
    }
}