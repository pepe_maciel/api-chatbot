module.exports = {
    attributes: {
        createdAt: false,
        updatedAt: false,
        codigoOcorrencia: {
            model: 'Ocorrencia',
            columnName: 'Código da Ocorrência',
            required: true
        },
        tipoOcorrencia: {
            type: 'string',
            required: true
        },
        categoriaOcorrencia: {
            type: 'string',
            required:true
        },
        taxonomiaOcorrencia: {
            type: 'string',
            required: true
        }
    }
}