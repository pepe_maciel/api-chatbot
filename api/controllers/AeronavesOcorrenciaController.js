module.exports = {
    async create(req, res) {
        try {
            let { codigoOcorrencia, aeronaveMatricula, aeronaveTipo, aeronaveFabricante, aeronaveModelo, aeronaveAno, aeronavePaisFabricante, aeronaveSegmento, aeronaveFaseOperacao } = req.allParams();

            if(!codigoOcorrencia || !aeronaveMatricula || !aeronaveTipo || !aeronaveFabricante || ! aeronaveModelo || !aeronaveAno || !aeronavePaisFabricante || !aeronaveSegmento || !aeronaveFaseOperacao) {
                return res.badRequest({err: 'Atributo(os) obrigatório(os) de aeronaves não preenchidos!'})
            }

            const resultados = await AeronavesOcorrencia.create({
                codigoOcorrencia, aeronaveMatricula, aeronaveTipo, aeronaveFabricante, aeronaveModelo, aeronaveAno, aeronavePaisFabricante, aeronaveSegmento, aeronaveFaseOperacao
            }).fetch()

            return res.ok(resultados)
        } catch(err) {
            return res.serverError(err);
        }
    },

    async find(req, res) {
        try {
            const aeronaves = await AeronavesOcorrencia.find().populate('codigoOcorrencia');
            return res.ok(aeronaves)
        } catch(err) {
            return res.serverError(err)
        }
    },

    async findOne(req, res) {
        try {
            const aeronave = await AeronavesOcorrencia.findOne({codigoOcorrencia: req.params.id})
            return res.ok(aeronave)
        } catch(err) {
            return res.serverError(err)
        }
    }
}