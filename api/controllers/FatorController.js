module.exports = {
    async create(req, res) {
        try {
            let { codigoOcorrencia, nomeFator, aspectoFator, fatorCondicionante, fatorArea } = req.allParams();

            if(!codigoOcorrencia || !nomeFator || !aspectoFator || !fatorCondicionante || !fatorArea) {
                return res.badRequest({err: 'Atributo(os) obrigatório(os) não preenchido(os)!'})
            }

            const results = await FatorOcorrencia.create({
                codigoOcorrencia, nomeFator, aspectoFator, fatorCondicionante, fatorArea
            }).fetch()

            return res.ok(results)
        } catch(err) {
            return res.serverError(err);
        }
    },

    async find(req, res) {
        try {
            const fatores = await FatorOcorrencia.find().populate('codigoOcorrencia')
            return res.ok(fatores)
        } catch(err) {
            return res.serverError(err);
        }
    },

    async findOne(req, res) {
        try {
            const fator = await FatorOcorrencia.findOne({codigoOcorrencia: req.params.id});
            return res.ok(fator)
        } catch(err) {
            return res.serverError(err)
        }
    }
}