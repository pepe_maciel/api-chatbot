module.exports = {
    async create(req, res) {
        try {
            let { codigoOcorrencia, recomendacaoNumero, recomendacaoConteudo, recomendacaoDestinatario } = req.allParams();
            
            if(!codigoOcorrencia || !recomendacaoNumero || !recomendacaoConteudo || !recomendacaoDestinatario) {
                return res.badRequest({err: 'Atributo(os) obrigatório(os) não preenchido(os)!'})
            }

            const resultados = await RecomendacaoOcorrencia.create({
                codigoOcorrencia, recomendacaoNumero, recomendacaoConteudo, recomendacaoDestinatario
            }).fetch();

            return res.ok(resultados)
        } catch (err) {
            return res.serverError(err)
        }
    },

    async find(req, res) {
        try {
            const recomendacoes = await RecomendacaoOcorrencia.find().populate('codigoOcorrencia')
            return res.ok(recomendacoes)
        } catch(err) {
            return res.serverError(err)
        }
    },

    async findOne(req, res) {
        try {
            const recomendacao = await RecomendacaoOcorrencia.findOne({codigoOcorrencia: req.params.id})
            return res.ok(recomendacao)
        } catch(err) {
            return res.serverError(err)
        }
    }
}