module.exports = {
    async create(req, res) {
        try{
            // Atributos referentes ao tipo da ocorrência e à ocorrência em si
            let { codigoOcorrencia, classificacaoOcorrencia, cidadeOcorrencia, ufOcorrencia, paisOcorrencia, dataOcorrencia, horaOcorrencia, relatorioDivulgado, recomendacoesRelatorio, quantidadeAeronaves } = req.allParams();
            
            // Verificação das ocorrências
            if(!codigoOcorrencia || !classificacaoOcorrencia || !cidadeOcorrencia || !ufOcorrencia || !paisOcorrencia || !dataOcorrencia || !horaOcorrencia || !relatorioDivulgado || !recomendacoesRelatorio || !quantidadeAeronaves) {
                return res.badRequest({err: 'Atributo(os) obrigatório(os) de ocorrência não preenchidos!'})
            }
 
            const ocorrencias = await Ocorrencia.create({
                codigoOcorrencia, classificacaoOcorrencia, cidadeOcorrencia, ufOcorrencia, paisOcorrencia, 
                dataOcorrencia, horaOcorrencia, relatorioDivulgado, recomendacoesRelatorio, quantidadeAeronaves
            }).fetch();
        
            return res.ok(ocorrencias);
        
        } catch(err) {
            return res.serverError(err)
        }

    },
    
    async find(req, res) {
        try {
            const ocorrencias = await Ocorrencia.find()
            .populate('fatores')
            .populate('aeronaves')
            .populate('recomendacoes')
            .populate('tipos')
            
            return res.ok(ocorrencias);
        } catch (err) {
            return res.serverError(err)
        }
    },

    async findOne(req, res) {
        try {
            const ocorrencia = await Ocorrencia.findOne({codigoOcorrencia: req.params.id});
            return res.ok(ocorrencia);
        } catch(err) {
            return res.serverError(err);
        }
    }
}