module.exports = {
    async create(req, res) {
        try{
            let { codigoOcorrencia, tipoOcorrencia, categoriaOcorrencia, taxonomiaOcorrencia } = req.allParams();

            if(!codigoOcorrencia || !tipoOcorrencia || !categoriaOcorrencia || !taxonomiaOcorrencia) {
                return res.badRequest({err: 'Codigo da ocorrência é um atributo obrigatório'})
            }

            const results = await TipoOcorrencia.create({
                codigoOcorrencia, tipoOcorrencia, categoriaOcorrencia, taxonomiaOcorrencia
            }).fetch();
            
            return res.ok(results)
        } catch(err) {
            return res.serverError(err)
        }
    },

    async find(req, res) {
        try {
            const tipoOcorrencia = await TipoOcorrencia.find().populate('codigoOcorrencia');
            return res.ok(tipoOcorrencia)
        } catch (err) {
            return res.serverError(err)
        }
    },

    async findOne(req, res) {
        try {
            const tipoOcorrencia = await TipoOcorrencia.findOne({codigoOcorrencia: req.params.id});
            return res.ok(tipoOcorrencia)
        } catch(err) {
            return res.serverError(err)
        }
    }
}