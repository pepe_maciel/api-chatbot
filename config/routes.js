/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
    // Rotas para Tipos das Ocorrências
    'POST /tiposocorrencia': 'TipoOcorrenciaController.create',
    'GET /tiposocorrencia': 'TipoOcorrenciaController.find',
    'GET /tiposocorrencia/:id': 'TipoOcorrenciaController.findOne',

    // Rotas para Ocorrências Aéreas
    'POST /ocorrencias': 'OcorrenciaController.create',
    'GET /ocorrencias': 'OcorrenciaController.find',
    'GET /ocorrencias/:id': 'OcorrenciaController.findOne',

    // Rotas para Fatores das Ocorrências
    'POST /fatores': 'FatorController.create',
    'GET /fatores': 'FatorController.find',
    'GET /fatores/:id': 'FatorController.findOne',

    // Rotas para Recomendações das Ocorrências
    'POST /recomendacoes': 'RecomendacaoOcorrenciaController.create',
    'GET /recomendacoes': 'RecomendacaoOcorrenciaController.find',
    'GET /recomendacoes/:id': 'RecomendacaoOcorrenciaController.findOne',

    // Rotas para Aeronaves das Ocorrências
    'POST /aeronaves': 'AeronavesOcorrenciaController.create',
    'GET /aeronaves': 'AeronavesOcorrenciaController.find',
    'GET /aeronaves/:id': 'AeronavesOcorrenciaController.findOne',
};
